<?php
/**
 * @file
 * pp_socials.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_socials_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
